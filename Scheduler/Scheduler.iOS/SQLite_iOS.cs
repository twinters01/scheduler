using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Scheduler;
using Xamarin.Forms;
using Scheduler.iOS;
using System.IO;
using SQLite.Net;

[assembly: Dependency (typeof (SQLite_iOS))]
namespace Scheduler.iOS
{
    class SQLite_iOS : ISQLite
    {
        public SQLite_iOS()
        { }

        public SQLiteConnection GetConnection()
        {
            var sqliteFilename = "TodoSQLite.db3";
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryPath = Path.Combine(documentsPath, "..", "Library");
            var path = Path.Combine(libraryPath, sqliteFilename);


            if(!File.Exists(path))
            {
                File.Copy(sqliteFilename, path);
            }

            var conn = new SQLiteConnection(new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS(), path);

            return conn;
        }
    }
}