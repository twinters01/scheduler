﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Scheduler.Views;
using Scheduler.Model;

namespace Scheduler
{
    public class App : Application
    {
        public enum DayOfWeek
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
        static Database db;
        public App()
        {
            // The root page of your application
            MainPage = new NavigationPage(new DeptListPage());
        }

        public static Database Db
        {
            get
            {
                if(db == null)
                {
                    db = new Database();
                }
                return db;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes from sleep
        }
    }
}
