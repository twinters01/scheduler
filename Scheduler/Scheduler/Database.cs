﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Scheduler.Model;
using System.Collections.ObjectModel;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensions.Attributes;
using SQLite.Net;

namespace Scheduler
{
    public class Database
    {
        static object locker = new object();
        SQLiteConnection db;

        public Database()
        {
            db = DependencyService.Get<ISQLite>().GetConnection();
            //A table containing the departments previously saved, subsequently all data
            db.CreateTable<Department>();
            db.CreateTable<Employee>();
            db.CreateTable<Schedule>();
            db.CreateTable<AvailabilityRule>();
            lock (locker)
            {
                if ((from i in db.Table<Department>() select i).ToList<Department>().Count == 0)
                {
                    Department dept1 = new Department(){ name = "dept1" };
                    db.Insert(dept1);

                    Employee emp1 = new Employee() { name = "emp1" };
                    emp1.deptID = dept1.ID;
                    db.Insert(emp1);

                    AvailabilityRule rule = new AvailabilityRule();
                    rule.empID = emp1.ID;
                    db.Insert(rule);

                    db.Insert(
                        new Department()
                        {
                            name = "Dept2"
                        }

                        );
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////         GETTERS                               //////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Gets the children of an object
        public void GetChildren(object obj)
        {
            lock(locker)
            {
                db.GetChildren(obj);
            }
        }

        //Gets all departments in the database
        public ObservableCollection<Department> GetDepts()
        {
            lock(locker)
            {
                //Return all departments from the db
                ObservableCollection<Department> depts = new ObservableCollection<Department>((from i in db.Table<Department>() select i));
                if (depts == null)
                    return new ObservableCollection<Department>();
                return depts;
            }
        }

        //Gets all employees associated with a specified department
        public ObservableCollection<Employee> GetEmployeesFromDept(int deptID)
        {
            lock (locker)
            {
                ObservableCollection<Employee> emps = new ObservableCollection<Employee>((from i in db.Table<Employee>() where i.department.ID == deptID select i).ToList());
                if (emps == null)
                    return new ObservableCollection<Employee>();
                return emps;
            }
        }

        //Gets the availability restrictions that belong to a specified employee
        public ObservableCollection<AvailabilityRule>GetAvailability(int employeeID)
        {
            lock(locker)
            {
                ObservableCollection<AvailabilityRule> rules = new ObservableCollection<AvailabilityRule>((from i in db.Table<AvailabilityRule>() where i.employee.ID == employeeID select i).ToList());
                if (rules == null)
                    return new ObservableCollection<AvailabilityRule>();
                return rules;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////         SAVERS                                //////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        //Saves a department to the table, returning the ID of that department
        public int SaveDept(Department dept)
        {
            lock(locker)
            {
                //If the department has an incremented ID already, this is an update
                if(dept.ID != 0)
                {
                    db.Update(dept);
                    return dept.ID;
                }
                //Otherwise we're saving a new department
                else
                {
                    return db.Insert(dept);
                }
            }
        }

        public int SaveEmp(Employee emp)
        {
            lock (locker)
            {
                if (emp.ID != 0)
                {
                    db.Update(emp);
                    return emp.ID;
                }
                //New employee
                else
                {
                    emp.department.employees.Add(emp);
                    db.UpdateWithChildren(emp.department);
                    return db.Insert(emp);
                }
            }
        }
        public int SaveAvailabilityRule(AvailabilityRule rule)
        {
            lock (locker)
            {
                if (rule.ID != 0)
                {
                    db.Update(rule);
                    return rule.ID;
                }
                else
                {
                    return db.Insert(rule);
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////         DELETERS                              //////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //TODO IMPLEMENT CASCADING
        //Remove a department from the database, returning the ID of that dept
        public int DeleteDept(int id)
        {
            lock(locker)
            {

                return db.Delete<Department>(id);
            }
        }

        //Remove an employee from the database, returning the ID of that employee
        public int DeleteEmp(int id)
        {
            lock (locker)
            {
                return db.Delete<Employee>(id);
            }
        }
        //Remove an availabilityRule from the database, returning the ID of that rule
        public int DeleteRule(int id)
        {
            lock(locker)
            {
                return db.Delete<AvailabilityRule>(id);
            }
        }
    }
}
