﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Scheduler.Model;
using Scheduler.Views.Templates;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Scheduler.Views
{
    public class EmployeeListPage : ContentPage
    {
        private ListView empList;

        public EmployeeListPage()
        {
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var layout = new StackLayout();
            Title = (BindingContext as Department).name + " Employees";

            //The layout that will contain the buttons
            var buttonSection = new StackLayout
            {
                Orientation = StackOrientation.Horizontal
            };
            //Add button
            var addButton = new Button
            {
                Text = "Add"
            };
            //Delete button
            var delButton = new Button
            {
                Text = "Delete",
                IsEnabled = false
            };
            delButton.Clicked += (sender, e) =>
            {
                (empList.ItemsSource as ObservableCollection<Employee>).Remove(empList.SelectedItem as Employee);
            };
            //Edit button
            var editButton = new Button
            {
                Text = "Edit",
                IsEnabled = false
            };

            //Next button
            var nextButton = new Button
            {
                Text = "Next",
                IsEnabled = false
            };

            empList = new ListView();
            empList.ItemTemplate = new DataTemplate(typeof(EmployeeCell));

            //Link the list of employees to the database
            ObservableCollection<Employee> source;
            if ((BindingContext as Department).employees == null)
                source = new ObservableCollection<Employee>();
            else
                source = new ObservableCollection<Employee>((BindingContext as Department).employees);
            //Set up the event to update the database if an item is deleted
            source.CollectionChanged += (sender, e) =>
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (Employee emp in e.OldItems)
                    {
                        App.Db.DeleteEmp(emp.ID);
                    }
                }
            };
            empList.ItemsSource = source;
            empList.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    editButton.IsEnabled = false;
                    nextButton.IsEnabled = false;
                    delButton.IsEnabled = false;
                }
                else
                {
                    editButton.IsEnabled = true;
                    nextButton.IsEnabled = true;
                    delButton.IsEnabled = true;
                }
            };

            //////////////////////////////////////////////////////////////
            //      EVENTS                                              //
            //////////////////////////////////////////////////////////////

            editButton.Clicked += onEditButtonClicked;
            addButton.Clicked += onAddButtonClicked;
            nextButton.Clicked += onNextButtonClicked;

            //Add buttons to the button layout
            buttonSection.Children.Add(addButton);
            buttonSection.Children.Add(delButton);
            buttonSection.Children.Add(editButton);
            buttonSection.Children.Add(nextButton);

            //Add everything to the main layout
            layout.Children.Add(empList);
            layout.Children.Add(buttonSection);
            layout.VerticalOptions = LayoutOptions.FillAndExpand;

            Content = layout;
        }

        private void onEditButtonClicked(Object sender, EventArgs e)
        {
            //Get the children of the selected object
            App.Db.GetChildren(empList.SelectedItem as Employee);

            EditEmployeePage editPage = new EditEmployeePage() { BindingContext = empList.SelectedItem as Employee };
            Navigation.PushAsync(editPage);
            int test = 2;
        }
        private void onAddButtonClicked(Object sender, EventArgs e) 
        {
            //Create a new employee linked to the current Department
            Employee newEmp = new Employee()
            {
                department = BindingContext as Department,
                deptID = (BindingContext as Department).ID
            };
            //Create an Employee Edit page linked to the new Employee
            EditEmployeePage addPage = new EditEmployeePage() { BindingContext = newEmp };
            Navigation.PushAsync(addPage);
            App.Db.GetChildren(BindingContext as Department);
        }
        private void onNextButtonClicked(Object sender, EventArgs e)
        {
        }
    }
}
