﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Scheduler.Model;

using Xamarin.Forms;

namespace Scheduler.Views
{
    public class EditAvailabilityRulePage : ContentPage
    {
        public EditAvailabilityRulePage()
        {
            
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            //todo differentiate single employee and list of employees
            AvailabilityRule rule = (BindingContext as Dictionary<String, object>)["rule"] as AvailabilityRule;

            var emps = (BindingContext as Dictionary<String, object>)["employee"];
            
            Entry reasonEntry = new Entry();
            reasonEntry.Text = rule.reason;

            DayPicker dayPicker = new DayPicker();
            dayPicker.SelectedIndex = Convert.ToInt32(rule.day);

            TimePicker shiftStartPicker = new TimePicker();
            shiftStartPicker.Time = TimeSpan.FromTicks(rule.shiftStart);
            
            TimePicker shiftEndPicker = new TimePicker();
            shiftEndPicker.Time = TimeSpan.FromTicks(rule.shiftEnd);
            
            Picker typeEntry = new Picker();
            typeEntry.Items.Add("Must work");
            typeEntry.Items.Add("Cannot work");
            typeEntry.SelectedIndex = Convert.ToInt32(rule.type);

            
            Picker employeePicker = new Picker();
            if (emps.GetType() == typeof(List<Employee>))
            {
                foreach (Employee e in emps as List<Employee>)
                {
                    employeePicker.Items.Add(e.name);
                }
            }
            Button saveButton = new Button();
            saveButton.Text = "Save";
            saveButton.Clicked += (sender, e) =>
            {
                rule.reason = reasonEntry.Text;
                rule.day = dayPicker.SelectedDay();
                rule.shiftStart = shiftStartPicker.Time.Ticks;
                rule.shiftEnd = shiftEndPicker.Time.Ticks;
                rule.type = (AvailabilityRule.RuleType)typeEntry.SelectedIndex;

                if(emps.GetType() == typeof(List<Employee>))
                {
                    foreach(Employee emp in emps as List<Employee>)
                    {
                        if (employeePicker.Items[employeePicker.SelectedIndex].Equals(emp.name))
                            onSaveButtonClicked(rule, emp);
                    }
                }
                else
                    onSaveButtonClicked(rule, emps as Employee);

                Navigation.PopAsync();
            };
            StackLayout layout = new StackLayout();
            layout.Children.Add(new Label() { Text = "Reason: " });
            layout.Children.Add(reasonEntry);
            layout.Children.Add(new Label() { Text = "Day: " });
            layout.Children.Add(dayPicker);
            layout.Children.Add(new Label() { Text = "Start time: " });
            layout.Children.Add(shiftStartPicker);
            layout.Children.Add(new Label() { Text = "End time: " });
            layout.Children.Add(shiftEndPicker);
            layout.Children.Add(new Label() { Text = "Type: " });
            layout.Children.Add(typeEntry);
            if(emps.GetType() == typeof(List<Employee>))
            {
                layout.Children.Add(new Label() { Text = "Employee: " });
                layout.Children.Add(employeePicker);
            }
            layout.Children.Add(saveButton);
            Content = layout;
        }
        protected virtual void onSaveButtonClicked(AvailabilityRule rule, Employee emp)
        {

        }
    }
}
