﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using Scheduler.Views.Templates;
using Scheduler.Model;
using System.Collections.ObjectModel;

namespace Scheduler.Views
{
    public class DeptListPage : ContentPage
    {
        private ListView deptList;
        public DeptListPage()
        {
            var layout = new StackLayout();

            Title = "Departments";
            
            //The layout that will contain the buttons
            var buttonSection = new StackLayout
            {
                Orientation = StackOrientation.Horizontal
            };
            //Add button
            var addButton = new Button
            {
                Text = "Add"
            };
            //Delete button
            var delButton = new Button
            {
                Text = "Delete",
                IsEnabled = false
            };
            delButton.Clicked += (sender, e) =>
            {
                (deptList.ItemsSource as ObservableCollection<Department>).Remove(deptList.SelectedItem as Department);
            };
            //Edit button
            var editButton = new Button
            {
                Text = "Edit",
                IsEnabled = false
            };
            
            //Next button
            var nextButton = new Button
            {
                Text = "Next",
                IsEnabled = false
            };

            deptList = new ListView();
            //Each cell on the listview will be defined by DeptCell
            deptList.ItemTemplate = new DataTemplate(typeof(DeptCell));
            deptList.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    editButton.IsEnabled = false;
                    nextButton.IsEnabled = false;
                    delButton.IsEnabled = false;
                }
                else
                {
                    editButton.IsEnabled = true;
                    nextButton.IsEnabled = true;
                    delButton.IsEnabled = true;
                }
            };

            //////////////////////////////////////////////////////////////
            //      EVENTS                                              //
            //////////////////////////////////////////////////////////////
            
            editButton.Clicked += onEditButtonClicked;
            addButton.Clicked += onAddButtonClicked;
            nextButton.Clicked += onNextButtonClicked;

            //Add buttons to the button layout
            buttonSection.Children.Add(addButton);
            buttonSection.Children.Add(delButton);
            buttonSection.Children.Add(editButton);
            buttonSection.Children.Add(nextButton);


            //Add the views to the layout
            layout.Children.Add(deptList);
            layout.Children.Add(buttonSection);
            layout.VerticalOptions = LayoutOptions.FillAndExpand;

            Content = layout;
        }

        async void onEditButtonClicked(object sender, EventArgs e)
        {
            //This page will be bound to the selected department
            EditDeptPage editPage = new EditDeptPage() { BindingContext = deptList.SelectedItem as Department };
            await Navigation.PushAsync(editPage);
        }

        async void onAddButtonClicked(object sender, EventArgs e)
        {
            //This page will be bound to a new department that will be saved to the DB
            EditDeptPage addPage = new EditDeptPage() { BindingContext = new Department() };
            await Navigation.PushAsync(addPage);
        }

        async void onNextButtonClicked(object sender, EventArgs e)
        {
            //Get the children of the selected department
            App.Db.GetChildren(deptList.SelectedItem as Department);

            EmployeeListPage empListPage = new EmployeeListPage() { BindingContext = deptList.SelectedItem as Department };
            await Navigation.PushAsync(empListPage);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            //Link the list of departments to the database
            ObservableCollection<Department> source = App.Db.GetDepts();
            source.CollectionChanged += (sender, e) =>
            {
                if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (Department d in e.OldItems)
                    {
                        App.Db.DeleteDept(d.ID);
                    }
                }
            };
            deptList.ItemsSource = source;
        }
    }
}
