﻿using Scheduler.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace Scheduler.Views
{
    public class EditDeptPage : ContentPage
    {
        public EditDeptPage()
        {
            
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Department dept = BindingContext as Department;

            //The text field that will contain the name
            Entry nameEntry = new Entry();
            nameEntry.Text = dept.name;

            Button saveButton = new Button();
            saveButton.Text = "Save";
            saveButton.Clicked += (sender, e) =>
            {
                dept.name = nameEntry.Text;
                App.Db.SaveDept(dept);
                Navigation.PopAsync();
            };

            Content = new StackLayout
            {
                Children = {
                    new Label() {Text="Name: " },
                    nameEntry,
                    saveButton
                }
            };
        }
    }
}
