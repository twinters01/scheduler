﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
namespace Scheduler.Views
{
    class DayPicker:Picker
    {
        public DayPicker()
        {
            Items.Add("Monday");
            Items.Add("Tuesday");
            Items.Add("Wednesday");
            Items.Add("Thursday");
            Items.Add("Friday");
            Items.Add("Saturday");
            Items.Add("Sunday");
        }

        public DayOfWeek SelectedDay()
        {
            switch(SelectedIndex)
            {
                case 0:
                    return DayOfWeek.Monday;
                case 1:
                    return DayOfWeek.Tuesday;
                case 2:
                    return DayOfWeek.Wednesday;
                case 3:
                    return DayOfWeek.Thursday;
                case 4:
                    return DayOfWeek.Friday;
                case 5:
                    return DayOfWeek.Saturday;
                case 6:
                    return DayOfWeek.Sunday;
                default:
                    return DayOfWeek.Monday;
            }
        }
    }
}
