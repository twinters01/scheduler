﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Scheduler.Model;
using Xamarin.Forms;

namespace Scheduler.Views
{
    public class AddAvailabilityRulePage : EditAvailabilityRulePage
    {
        protected override void onSaveButtonClicked(AvailabilityRule rule, Employee emp)
        {
            emp.rules.Add(rule);
        }
    }
}
