﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Scheduler.Model;

using Xamarin.Forms;

namespace Scheduler.Views
{
    public class EditEmployeePage : ContentPage
    {
        private Employee emp = null;
        public EditEmployeePage()
        {
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if(emp == null)
                emp = BindingContext as Employee;

            //The text field that will contain the name
            Entry nameEntry = new Entry();
            nameEntry.Text = emp.name;

            //The text field that will contain the name
            Entry minDaysEntry = new Entry();
            minDaysEntry.Text = emp.minDays.ToString();

            //The text field that will contain the name
            Entry maxDaysEntry = new Entry();
            maxDaysEntry.Text = emp.maxDays.ToString();

            //The text field that will contain the name
            Entry minHoursEntry = new Entry();
            minHoursEntry.Text = emp.minHours.ToString();

            //The text field that will contain the name
            Entry maxHoursEntry = new Entry();
            maxHoursEntry.Text = emp.maxHours.ToString();

            Button saveButton = new Button();
            saveButton.Text = "Save";
            saveButton.Clicked += (sender, e) =>
            {
                //Save the employee
                emp.name = nameEntry.Text;
                emp.minDays = int.Parse(minDaysEntry.Text);
                emp.maxDays = int.Parse(maxDaysEntry.Text);
                emp.minHours = int.Parse(minHoursEntry.Text);
                emp.maxHours = int.Parse(maxHoursEntry.Text);
                App.Db.SaveEmp(emp);

                //Now save and associate the (possibly new) availability rules with the (possibly new) employee
                foreach (AvailabilityRule rule in emp.rules)
                {
                    rule.empID = emp.ID;
                    App.Db.SaveAvailabilityRule(rule);
                }

                Navigation.PopAsync();
            };

            Button availabilityButton = new Button();
            availabilityButton.Text = "Availability";
            availabilityButton.Clicked += OnAvailabilityClicked;

            Content = new StackLayout
            {
                Children = {

                    new Label() { Text = "Name: "},
                    nameEntry,
                    new Label() { Text = "Minimum Days: "},
                    minDaysEntry,
                    new Label() { Text = "Maximum Days: "},
                    maxDaysEntry,
                    new Label() { Text = "Minimum Hours: "},
                    minHoursEntry,
                    new Label() { Text = "Maximum Hours: "},
                    maxHoursEntry,
                    saveButton,
                    availabilityButton
                }
            };
        }
        async private void OnAvailabilityClicked(object sender, EventArgs e)
        {
            EmployeeAvailabilityListPage availPage = new EmployeeAvailabilityListPage();
            availPage.BindingContext = emp;
            await Navigation.PushAsync(availPage);
        }
    }
}
