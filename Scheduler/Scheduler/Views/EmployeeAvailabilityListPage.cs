﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Scheduler.Model;
using Scheduler.Views.Templates;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Scheduler.Views
{
    public class EmployeeAvailabilityListPage : ContentPage
    {
        private ListView availList;
        public EmployeeAvailabilityListPage()
        {
            
        } 

        async void onEditButtonClicked(object sender, EventArgs e)
        {
            //This page will be bound ot a new AvailabilityRule that will be saved to the DB
            EditAvailabilityRulePage editPage = new EditAvailabilityRulePage();
            //Binds the page to a dictionary which contains the employee we're working with and the selected rule
            Dictionary<String, Object> binding = new Dictionary<string, object>();
            binding["rule"] = availList.SelectedItem as AvailabilityRule;
            binding["employee"] = BindingContext as Employee;
            editPage.BindingContext = binding;

            await Navigation.PushAsync(editPage);
        }

        async void onAddButtonClicked(object sender, EventArgs e)
        {
            //This page will be bound ot a new AvailabilityRule that will be saved to the DB
            AddAvailabilityRulePage addPage = new AddAvailabilityRulePage();
            //Binds the page to a dictionary which contains the employee we're working with and the rule which will be attached to them
            Dictionary<String, Object> binding = new Dictionary<string, object>();
            binding["rule"] = new AvailabilityRule() { employee = BindingContext as Employee};
            binding["employee"] = BindingContext as Employee;
            addPage.BindingContext = binding;

            await Navigation.PushAsync(addPage);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            Title = (BindingContext as Employee).name + "'s Availability Restrictions";

            var layout = new StackLayout();

            //The layout that will contain the buttons
            var buttonSection = new StackLayout
            {
                Orientation = StackOrientation.Horizontal
            };
            //Add button
            var addButton = new Button
            {
                Text = "Add"
            };
            //Delete button
            var delButton = new Button
            {
                Text = "Delete",
                IsEnabled = false
            };
            delButton.Clicked += (sender, e) =>
            {
                (availList.ItemsSource as ObservableCollection<AvailabilityRule>).Remove(availList.SelectedItem as AvailabilityRule);
            };
            //Edit button
            var editButton = new Button
            {
                Text = "Edit",
                IsEnabled = false
            };

            availList = new ListView();
            availList.ItemTemplate = new DataTemplate(typeof(AvailCell));
            availList.ItemSelected += (sender, e) =>
            {
                if (e.SelectedItem == null)
                {
                    editButton.IsEnabled = false;
                    delButton.IsEnabled = false;
                }
                else
                {
                    editButton.IsEnabled = true;
                    delButton.IsEnabled = true;
                }
            };

            //Link the list view to the employee's availability
            if((BindingContext as Employee).rules == null)
                App.Db.GetChildren(BindingContext as Employee);
            ObservableCollection<AvailabilityRule> source = new ObservableCollection<AvailabilityRule>((BindingContext as Employee).rules);
            source.CollectionChanged += (sender, e) =>
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (AvailabilityRule a in e.OldItems)
                    {
                        App.Db.DeleteRule(a.ID);
                    }
                }
            };
            availList.ItemsSource = source;

            //////////////////////////////////////////////////////////////
            //      EVENTS                                              //
            //////////////////////////////////////////////////////////////

            editButton.Clicked += onEditButtonClicked;
            addButton.Clicked += onAddButtonClicked;

            //Add buttons to the button layout
            buttonSection.Children.Add(addButton);
            buttonSection.Children.Add(delButton);
            buttonSection.Children.Add(editButton);


            //Add the views to the layout
            layout.Children.Add(availList);
            layout.Children.Add(buttonSection);
            layout.VerticalOptions = LayoutOptions.FillAndExpand;

            Content = layout;
        }
    }
}
