﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace Scheduler.Views.Templates
{
    public class EmployeeCell : ViewCell
    {
        public EmployeeCell()
        {
            var nameLabel = new Label()
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            nameLabel.SetBinding(Label.TextProperty, "name");

            var layout = new StackLayout
            {
                Padding = new Thickness(20, 0, 20, 0),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { nameLabel }
            };
            View = layout;
        }
    }
}
