﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace Scheduler.Views.Templates
{
    public class AvailCell : ViewCell
    {
        public AvailCell()
        {
            var reasonLabel = new Label()
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            reasonLabel.SetBinding(Label.TextProperty, "reason");
            var layout = new StackLayout
            {
                Padding = new Thickness(20, 0, 20, 0),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { reasonLabel }
            };
            View = layout;
        }
    }
}
