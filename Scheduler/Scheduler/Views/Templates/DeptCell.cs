﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using Scheduler.Model;

namespace Scheduler.Views.Templates
{
    public class DeptCell : ViewCell
    {
        public DeptCell()
        {
            var nameLabel = new Label()
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };
            nameLabel.SetBinding(Label.TextProperty, "name");

            //Labels for the employee count
            var empCountLabel = new Label()
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Text = "Employees: "
            };
            var empCountDisplay = new Label()
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Text = "TODO"//(BindingContext as Department).getEmployeeCount().ToString()
            };

            var layout = new StackLayout
            {
                Padding = new Thickness(20, 0, 20, 0),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { nameLabel, empCountLabel, empCountDisplay }
            };
            View = layout;
        }
    }
}
