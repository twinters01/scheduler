﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace Scheduler.Model
{
    public class Schedule
    {
        [PrimaryKey,AutoIncrement]
        public int ID { set; get; }
        

        /// <summary>
        /// Foreign references
        /// </summary>
        
        //Reference to the department this schedule belongs to
        [ForeignKey(typeof(Department))]
        public int deptID { get; set; }
        [ManyToOne]
        public Department dept { get; set; }



        //The shifts in the schedule
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Shift> shifts { get; set; }
    }
}
