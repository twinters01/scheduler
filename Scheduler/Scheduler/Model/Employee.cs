﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;
using SQLite.Net.Attributes;

namespace Scheduler.Model
{
    public class Employee
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public String name { get; set; }
        //Minimum number of days per week the employee will be allowed to work
        public int minDays { get; set; } = 5;
        //Maximum number of days per week the employee will be allowed to work
        public int maxDays { get; set; } = 7;
        //Minimum number of hours per week the employee will be allowed to work
        public int minHours { get; set; } = 25;
        //Maximum number of hours per week the employee will be allowed to work
        public int maxHours { get; set; } = 40;


        /// <summary>
        /// Foreign References
        /// </summary>

        //Reference to the department this employee belongs to
        [ForeignKey(typeof(Department))]
        public int deptID { get; set; }
        [ManyToOne, NotNull]
        public Department department { get; set; }

        //Reference to the availability rules associated with this employee
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<AvailabilityRule> rules { get; set; } = new List<AvailabilityRule>();
    }
}
