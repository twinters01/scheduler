﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace Scheduler.Model
{
    public class Shift
    {
        const int SHIFT_EMPTY = -1;


        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        
        public DayOfWeek day { get; set; }

        public long shiftStart { get; set; }

        public long shiftEnd { get; set; }

        [ForeignKey(typeof(Employee))]
        public int empID { set; get; } = SHIFT_EMPTY;
        [OneToOne(CascadeOperations = CascadeOperation.All)]
        public Employee employee { set; get; } = null;

        public bool isEmpty()
        {
            if (empID == SHIFT_EMPTY)
                return true;
            else
                return false;
        }
    }
}
