﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduler.Model
{
    public class Timeslot
    {
        public DayOfWeek day { get; set; }
        public long shiftStart { get; set; }
        public long shiftEnd { get; set; }
    }
}
