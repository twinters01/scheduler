﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace Scheduler.Model
{
    public class Department
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public String name { get; set; }


        /// <summary>
        /// Foreign References
        /// </summary>

        //Employees in the department
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Employee> employees { get; set; } = new List<Employee>();

        //Schedules saved to the department
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Schedule> schedules { get; set; } = new List<Schedule>();
        
    }
}
