﻿using System;
using System.Collections.Generic;
using SQLite.Net.Attributes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensions.Attributes;

namespace Scheduler.Model
{
    public class AvailabilityRule
    {
        public enum RuleType
        {
            Required,
            Unavailable
        }
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DayOfWeek day { get; set; } = DayOfWeek.Monday;
        public long shiftStart { get; set; } = new TimeSpan().Ticks;
        public long shiftEnd { get; set; } = new TimeSpan().Ticks;
        public RuleType type { get; set; } = RuleType.Unavailable;
        public String reason { get; set; }

        /// <summary>
        /// Foreign References
        /// </summary>
        
        //Employee that this rule belongs to
        [ForeignKey(typeof(Employee))]
        public int empID { get; set; }
        [ManyToOne(CascadeOperations = CascadeOperation.CascadeInsert)]
        public Employee employee { get; set; }
    }
}
