﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Scheduler;
using Scheduler.WinPhone;
using Xamarin.Forms;
using Windows.Storage;
using SQLite.Net;


[assembly: Dependency(typeof(SQLite_WinPhone81))]
namespace Scheduler.WinPhone
{
    class SQLite_WinPhone81:ISQLite
    {
        public SQLite_WinPhone81()
        {
        }

        public SQLiteConnection GetConnection()
        {
            var sqliteFilename = "SchedulerSQLite.db3";


            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);

            var conn = new SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            return conn;
        }
    }
}
