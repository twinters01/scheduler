using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Scheduler.Droid;
using System.IO;
using Scheduler;
using SQLite.Net;

[assembly: Dependency(typeof(SQLite_Android))]
namespace Scheduler.Droid
{
    public class SQLite_Android: ISQLite
    {
        public SQLiteConnection GetConnection()
        {   
            var sqliteFilename = "SchedulerSQLite.db";
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, sqliteFilename);
            
            var conn = new SQLiteConnection(new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid(), path);
            return conn;
        }
    }
}